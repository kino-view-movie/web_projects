$(function(){
 
});

let video = document.getElementById('video'),
    play = document.getElementById('play'),
    pause = document.getElementById('pause'),
    bar = document.getElementById('bar'),
    progress = document.getElementById('progress'),
    maxim = bar.offsetWidth;

function initiate() {

        play.addEventListener('click', push, false);
        pause.addEventListener('click', push, false);
        bar.addEventListener('click', move, false);

}

function push(){

    if(!video.paused && !video.ended) {
        video.pause();
        window.clearInterval(loop);
    }else{
        video.play();
        loop=setInterval(status, 1000);
    }
}

function status(){
    if(!video.ended){
        let size = parseInt(video.currentTime*maxim/video.duration);
        progress.style.width = size+'px';
    }else{
        progress.style.width = '0px';
        window.clearInterval(loop);
    }
}

function move(e){
    if(!video.ended){
        let mouseX = e.pageX - bar.offsetLeft;
        progress.style.width = mouseX + 'px';
        var newtime = mouseX*video.duration/maxim;
        video.currentTime = newtime;
    }
}

window.addEventListener('load', initiate, false);